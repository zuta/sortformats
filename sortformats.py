#|/usr/bin/env python3

import sys

fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga", "pdf", "svg", "png", "gif", "jpeg", "webp")

def lower_than(format1: str, format2: str) -> bool:
    argumento1 = fordered.index(format1)
    argumento2 = fordered.index(format2)

    return argumento1 < argumento2

def find_lower_pos(formats: list, pivot: int) -> int:
    menor = pivot                                       #uso el numero menor de pivote como vimos en clase
    for i in range(pivot +1, len(formats)):             #el rango de pivot debe ser +1 porque sino sería el mismo, hasta el final de la lista len() devuelve la longitud de la lista
        if lower_than(formats[i], formats[menor]):      #aquí especificamos que si cada elemento de la lista formats es menor que el menor (pivote) de la misma lista diremos que el menor sera i.
            menor = i


    return menor

def sort_formats(formats: list) -> list:
    for i in range(len(formats)):
        menor = find_lower_pos(formats, i)
        formats[i], formats[menor] = formats[menor], formats[i]

    return formats


def main():

    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        print(format, end=" ")
    print()


if __name__ == '__main__':
    main()





