# |/usr/bin/env python3
import sys

fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga", "pdf", "svg", "png", "gif", "jpeg", "webp")

def lower_than(format1: str, format2: str) -> bool:
    argumento1 = fordered.index(format1)
    argumento2 = fordered.index(format2)

    return argumento1 < argumento2
